#ifndef TABLERO_H
#define TABLERO_H

#include <vector>
#include <string>
#include "Ficha.h"

using namespace std;

class Tablero{
public: 
    Tablero(int tamaño);
    vector<vector<string>> getTablero();
    void mostrarTablero();
    void mostrarTableroFalso();
    int getTamaño() const;
    string getValor(int i,int j);
    void ponerFicha(int i, int j ,string valor);
    void colocarFicha(Ficha f);
    char disparoTablero(int i, int j);
    bool terminoJuego();
private:
    int tamaño;
    vector<vector<string>> tablero;
    Ficha portaaviones;
    Ficha buque;
    Ficha submarino;
    Ficha lancha;
};
#endif