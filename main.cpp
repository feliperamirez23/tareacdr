#include <iostream>
#include "Tablero.h"
#include "Ficha.h"
#include <cstdlib>
#include <unistd.h>
#include <cctype>

using namespace std;

int main(int argc, char* argv[]){
    Tablero tableroJ1(15);
    Tablero tableroJ2(15);
    //Tablero tableroJ2(15);
    tableroJ2.mostrarTableroFalso();
    tableroJ1.mostrarTablero();
    int disparosrealizados[tableroJ1.getTamaño()][tableroJ1.getTamaño()];
    for (int i = 0; i < tableroJ1.getTamaño(); ++i) {
        for (int j = 0; j < tableroJ1.getTamaño(); ++j) {
            disparosrealizados[i][j] = 0;
        }
    }
    //tableroJ2.mostrarTablero();
    int intentos=0;
    do
    {
    string disparo;
    int x;
    char y;
    bool valido=false;
    do
    {
        cout << "Ingresa las coordenadas de disparo separadas por espacios (ej. 4 D)" << endl;
        getline(std::cin,disparo);
        
        if (!isdigit(disparo[0])) {
            valido = false;
            cout << "Disparo no válido (el primer valor es un número)" << endl;
            break;
        }
        if (!isalpha(disparo[disparo.length() - 1]))
        {
            valido = false;
            cout << "Disparo no válido (el segundo valor es una letra)" << endl;
            break;
        }
        x=stoi(disparo.substr(0,disparo.find_first_of(" ")));
        y=(char)(disparo.substr(disparo.length()-1,disparo.length())).c_str()[0];
        if (x>tableroJ1.getTamaño() || y>64+tableroJ1.getTamaño())
        {
            valido=false;
            cout << "Disparo no valido (excede el tableo)" << endl;
            break;
        }
        if (x<=0)
        {
            valido=false;
            cout << "Disparo no valido (Empieza desde el 1)" << endl;
            break;
        }
        if (y<65)
        {
            valido=false;
            cout << "Disparo no valido (Solo llega acepta hasta la letra O)" << endl;
            break;
        }
        if (disparo.length() <= 4) {
            if (x>tableroJ1.getTamaño())
            {
            valido = false;
            cout << "Disparo no válido (se requiere un número y una letra)" << endl;
            break;
            }
        }
        valido=true;
        
        
    } while (!valido);
    
    if (!valido){
        continue;
    }
    
    
    tableroJ2.disparoTablero(x-1,(int)(y-64-1));
    int paso=0;
    do
    {
        srand(time(0));
        int xcpu =rand()% tableroJ1.getTamaño();
        int ycpu =rand()% tableroJ1.getTamaño();
        if(disparosrealizados[xcpu][ycpu]==0){
            tableroJ1.disparoTablero(xcpu,ycpu);
            disparosrealizados[xcpu][ycpu]=1;
            paso=1;
        }else{
            paso=0;
        }
    } while (paso==0);
    intentos+=1;
    system("clear");
    tableroJ2.mostrarTableroFalso();
    tableroJ1.mostrarTablero();
    } while (!tableroJ1.terminoJuego()|| !tableroJ2.terminoJuego());
    cout << "Ganaste en "<< intentos << " intentos " << endl;
    
    

}