#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdlib>
#include <cctype>
#include "Tablero.h"
#include "Ficha.h"

using namespace std;

const int BUFFER_SIZE = 4096;

int main(int argc, char *argv[]) {
    if (argc > 1) {
        int fd, fd2, longitud_cliente, puerto;
        puerto = atoi(argv[1]);
        
        struct sockaddr_in server;
        struct sockaddr_in client;
        
        server.sin_family = AF_INET;
        server.sin_port = htons(puerto);
        server.sin_addr.s_addr = INADDR_ANY;
        bzero(&(server.sin_zero), 8);
        
        if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            perror("Error al abrir socket");
            exit(-1);
        }
        
        if (bind(fd, (struct sockaddr*)&server, sizeof(struct sockaddr)) == -1) {
            cout << "Error en bind" << endl;
            exit(-1);
        }
        
        if (listen(fd, 5) == -1) {
            cout << "Error en listen" << endl;
        }
        
        while (1) {
            longitud_cliente = sizeof(struct sockaddr_in);
            
            if ((fd2 = accept(fd, (struct sockaddr *)&client, (socklen_t *)&longitud_cliente)) == -1) {
                cout << "Error en conexion" << endl;
                exit(-1);
            }
            
            char mensaje_envio[BUFFER_SIZE];
            char mensaje_envio2[BUFFER_SIZE];
            char mensaje_recepcion[BUFFER_SIZE];
            Tablero tserver(15);
            tserver.mostrarTablero();
            char valor;
            int con=0;
            int x;
            char y;
            int xcpu;
            int ycpu;
            int disparosrealizados[tserver.getTamaño()][tserver.getTamaño()];
            for (int i = 0; i < tserver.getTamaño(); ++i) {
                for (int j = 0; j < tserver.getTamaño(); ++j) {
                    disparosrealizados[i][j] = 0;
                }
            }
            while (!tserver.terminoJuego()) {
                // Enviar mensaje al cliente
                strcpy(mensaje_envio, "Ingrese Disparo");
                send(fd2, mensaje_envio, strlen(mensaje_envio), 0);
                // Recibir mensaje del cliente
                char mensaje_recepcion[BUFFER_SIZE];
                int numbytes = recv(fd2, mensaje_recepcion, BUFFER_SIZE - 1, 0);
                mensaje_recepcion[numbytes] = '\0';
                string mensaje(mensaje_recepcion);
                x=stoi(mensaje.substr(0,mensaje.find_first_of(" ")));
                y=(char)(mensaje.substr(mensaje.length()-1,mensaje.length())).c_str()[0];
                valor=tserver.disparoTablero(x-1,(int)(y-64-1));
                cout << "Mensaje recibido del cliente: " << mensaje << endl;
                tserver.mostrarTablero();
                int paso=0;
                do
                {
                    srand(time(0));
                    xcpu =rand()% tserver.getTamaño();
                    ycpu =rand()% tserver.getTamaño();
                    if(disparosrealizados[xcpu][ycpu]==0){
                        disparosrealizados[xcpu][ycpu]=1;
                        paso=1;
                    }else{
                        paso=0;
                    }
                } while (paso==0);
                if (valor=='X')
                {
                    snprintf(mensaje_envio2, BUFFER_SIZE, "%d %d Acertaste ", xcpu, ycpu);
                }else{
                    snprintf(mensaje_envio2, BUFFER_SIZE, "%d %d Fallaste ", xcpu, ycpu);
                }
                send(fd2, mensaje_envio2, strlen(mensaje_envio2), 0);
            }
            strcpy(mensaje_envio, "Ingrese Disparo");
            send(fd2, mensaje_envio, strlen(mensaje_envio), 0);
            close(fd2);
        }
        
        close(fd);
    } else {
        cout << "Error: No se ingresó el puerto" << endl;
    }
    
    return 0;
}
