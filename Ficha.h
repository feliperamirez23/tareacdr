#ifndef FICHA_H
#define FICHA_H
#include <string>
using namespace std;
class Ficha{
public:
    Ficha();
    Ficha(string nombre, int tamaño);
    void setNombre(string nombre);
    string getNombre() const;
    void setTamaño(int tamaño);
    int getTamaño() const;
private:
    string nombre;
    int tamaño;
};
#endif
