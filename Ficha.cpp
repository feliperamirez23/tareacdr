#include "Ficha.h"
using namespace std;
Ficha::Ficha(){
    nombre = "";
    tamaño = 0;
}

Ficha::Ficha(string nombre, int tamaño){
    this->nombre = nombre;
    this->tamaño = tamaño;
}
void Ficha::setNombre(string nombre){
    this->nombre = nombre;
}
string Ficha::getNombre() const {
    return nombre;
}
void Ficha::setTamaño(int tamaño){
    this->tamaño = tamaño;
}
int Ficha::getTamaño() const {
    return tamaño;
}