#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstdlib>
#include <cctype>
#include <sstream>
#include "Tablero.h"
#include "Ficha.h"
using namespace std;

const int BUFFER_SIZE = 4096;

int main(int argc, char *argv[]) {
    if (argc > 2) {
        char *ip;
        int fd, numbytes, puerto;
        char buf[BUFFER_SIZE];
        puerto = atoi(argv[2]);
        ip = argv[1];
        
        struct hostent *he;
        struct sockaddr_in server;
        
        if ((he = gethostbyname(ip)) == NULL) {
            cout << "gethostbyname error" << endl;
            exit(-1);
        }
        
        if ((fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            cout << "error de socket" << endl;
            exit(-1);
        }
        
        server.sin_family = AF_INET;
        server.sin_port = htons(puerto);
        server.sin_addr = *((struct in_addr*)he->h_addr);
        bzero(&(server.sin_zero), 8);
        
        if (connect(fd, (struct sockaddr*)&server, sizeof(server)) == -1) {
            cout << "connect error" << endl;
        }
        
        if ((numbytes = recv(fd, buf, BUFFER_SIZE - 1, 0)) == -1) {
            cout << "recv error" << endl;
        }
        
        buf[numbytes] = '\0';
        Tablero tcliente(15);
        tcliente.mostrarTablero();
        char valor;
        int w,s;
        int x,y;
        bool valido=false;
        string res,e,f;
        cout << buf << endl; 
        
        
        while (!tcliente.terminoJuego()) {
            // Ingresar mensaje desde teclado
            string mensaje_envio;
            do
            {
                getline(cin, mensaje_envio);
                
                if (!isdigit(mensaje_envio[0])) {
                valido = false;
                cout << "Disparo no válido (el primer valor es un número)" << endl;
                continue;
                }
                if (!isalpha(mensaje_envio[mensaje_envio.length() - 1]))
                {
                    valido = false;
                    cout << "Disparo no válido (el segundo valor es una letra)" << endl;
                    break;
                }
                x=stoi(mensaje_envio.substr(0,mensaje_envio.find_first_of(" ")));
                y=(char)(mensaje_envio.substr(mensaje_envio.length()-1,mensaje_envio.length())).c_str()[0];
                if (x>tcliente.getTamaño() || y>64+tcliente.getTamaño())
                {
                    valido=false;
                    cout << "Disparo no valido (excede el tableo)" << endl;
                    break;
                }
                if (x<=0)
                {
                    valido=false;
                    cout << "Disparo no valido (Empieza desde el 1)" << endl;
                    break;
                }
                if (y<65)
                {
                    valido=false;
                    cout << "Disparo no valido (Solo llega acepta hasta la letra O)" << endl;
                    break;
                }
                if (mensaje_envio.length() <= 4) {
                    if (x>tcliente.getTamaño())
                    {
                    valido = false;
                    cout << "Disparo no válido (se requiere un número y una letra)" << endl;
                    break;
                    }
                }
                valido = true;
            } while (!valido);
            if (!valido){
                continue;
            }
            
            // Enviar mensaje al servidor
            send(fd, mensaje_envio.c_str(), mensaje_envio.length(), 0);
            
            // Verificar si se debe finalizar el bucle
            
                // Recibir respuesta del servidor
                numbytes = recv(fd, buf, BUFFER_SIZE - 1, 0);
                buf[numbytes] = '\0';
                string disparoserver(buf);
                stringstream ss(buf);
                ss >> w >> s >> res >> e >> f;
                valor=tcliente.disparoTablero(w,s);
                tcliente.mostrarTablero();
                cout << res << endl << e << " " << f <<endl;
            
        }
        close(fd);
    } else {
        cout << "Error: No se ingresó la IP y el puerto" << endl;
    }
    
    return 0;
}
