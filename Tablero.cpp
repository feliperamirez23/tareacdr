#include "Tablero.h"
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

Tablero::Tablero(int tamaño){
    this->tamaño = tamaño;
    Ficha portaaviones("P",5);
    Ficha buque("B",4);
    Ficha submarino("S",3);
    Ficha lancha("L",1);
    tablero.resize(tamaño, vector<string>(tamaño));
    for (size_t i = 0; i < tamaño; i++){
        for (size_t j = 0; j < tamaño; j++)
        {
            tablero[i][j] ='-';
        }
    }
    cout << "Generando Tablero...." << endl;
    this->colocarFicha(portaaviones);
    this->colocarFicha(buque);
    this->colocarFicha(buque);
    this->colocarFicha(submarino);
    this->colocarFicha(submarino);
    this->colocarFicha(lancha);
    this->colocarFicha(lancha);
    this->colocarFicha(lancha);
    cout << "Listo" << endl;
}

vector<vector<string>> Tablero::getTablero(){
    return this->tablero;
}

int Tablero::getTamaño()const{
    return this->tamaño;
}

void Tablero::mostrarTablero(){
    cout << setw(3) << " " ;
    for (char i = 64; i < 64+tamaño; i++){
        cout << setw(3) << (char)(i+1);
    }
    cout << endl;
    for (size_t i = 0; i < tamaño; i++){
        cout << setw(3) << i+1;
        for (size_t j = 0; j < tamaño; j++)
        {
            cout << setw(3) << tablero[i][j];
        }
        cout << endl;
    }
    cout << endl;
}
void Tablero::mostrarTableroFalso(){
    cout << setw(3) << " " ;
    for (char i = 64; i < 64+tamaño; i++){
        cout << setw(3) << (char)(i+1);
    }
    cout << endl;
    for (size_t i = 0; i < tamaño; i++){
        cout << setw(3) << i+1;
        for (size_t j = 0; j < tamaño; j++)
        {
            if (tablero[i][j] == "X")
            {
                cout << setw(3) << tablero[i][j];    
            }else{
                if (tablero[i][j] == "|")
                {
                    cout << setw(3) << tablero[i][j];
                }else{
                    cout << setw(3) << "-";
                }
            }
        }
        cout << endl;
    }
    cout << endl;
}

void Tablero::ponerFicha(int i,int j,string valor){
    tablero[i][j] = valor;
}

string Tablero::getValor(int i,int j){
    return tablero[i][j];
}
void Tablero::colocarFicha(Ficha f){
    int fila_inicial;
    int col_inicial;
    int alineacion;
    int cabe;
    do
    {
        srand(time(0));
        fila_inicial =rand()% tamaño;
        col_inicial =rand()% tamaño;
        alineacion =rand()% 2;
        if (alineacion == 0)
        {
            if (fila_inicial+f.getTamaño()>tamaño)
            {
                cabe=0;
            }else{
                cabe=1;
            }
            
        }else{
            if (col_inicial+f.getTamaño()>tamaño)
            {
                cabe=0;
            }else{
                cabe=1;
            }
        }
        if (cabe == 1)
        {
            if (alineacion == 0) // vertical
            {
                for (size_t i = 0; i < f.getTamaño(); i++)
                {
                    if (tablero[fila_inicial+i][col_inicial]=="-")
                    {
                        cabe=1;
                    }else{
                        cabe=0;
                        break;
                    }  
                }
            }else{
                for (size_t i = 0; i < f.getTamaño(); i++)
                {
                    if (tablero[fila_inicial][col_inicial+i]=="-")
                    {
                        cabe=1;
                    }else{
                        cabe=0;
                        break;
                    }  
                }
            }
        }
    } while (cabe == 0);

    //cout << fila_inicial+1 << " " << col_inicial+1 << " " <<alineacion << endl;
    if (alineacion == 0) // vertical
    {
        for (size_t i = 0; i < f.getTamaño(); i++)
        {
            tablero[fila_inicial+i][col_inicial] = f.getNombre();
        }
    }else{
        for (size_t i = 0; i < f.getTamaño(); i++)
        {
            tablero[fila_inicial][col_inicial+i] = f.getNombre();
        }
    }
}
char Tablero::disparoTablero(int i , int j){
    if (tablero[i][j] == "-")
    {
        tablero[i][j] = "|";
        return '|';
    }else{
        if (tablero[i][j] == "|")
        {
            tablero[i][j] = "|";
            return '|';
        }else{
            tablero[i][j] = "X";
            return 'X';
        }
        
    }
    
}
bool Tablero::terminoJuego(){
    for (size_t i = 0; i < tamaño; i++){
        for (size_t j = 0; j < tamaño; j++)
        {
            if (tablero[i][j] == "P" || tablero[i][j] == "S" || tablero[i][j] == "B" 
                || tablero[i][j]=="L")
            {
                return false;
            }
        }
    }
    return true ;
}