# Tarea CDR
Jorge Fernández Enríquez
Felipe Ramírez Díaz
## Compilacion
Make servidor
Make cliente
## Anexo
Método de disparo estricto: un número + espacio + letra mayúscula.
Ejemplo: 2 B.
pdta: Cambio autorizado por profesor para Readme.md el día 29/05/2023 a las 11:23 hrs
y comprende que no se descontará puntos por la hora de entrega.
## Ejecucion
### en una terminal hacer
./servidor [puerto]

### E.J
./servidor 7777
### en otra terminal hacer
./cliente [ip] [puerto]
### E.J
./cliente 127.0.0.0 7777
